<?php

class Generic {

	
    /**
     * Create selector
     */
	public function selector($name, $selected, $options){
		$html = "<select name='".$name."'>\n";
		
		foreach($options as $k => $v) 
		{
			$html .= "<option value='" . $k . "'" . ($k==$selected?" selected":"") . ">$v</option>\n";
		}
		
		$html .= "</select>\n";
		
		return $html;
	}

    /**
     * Create time selector
     */
	public function timeSelector($selectedHour, $selectedMin, $prefix){
		$html = "<select name='".$prefix."_hour'>\n";
		for ($i=0;$i<24;$i++) 
		{
			$html .= "<option value='" . sprintf("%02d", $i) . "'" . ($i==$selectedHour?" selected":"") . ">$i</option>\n";
		}
		$html .= "</select>:<select name='".$prefix."_min'>\n";
		for ($i=0;$i<60;$i+=5) 
		{
			$html .= "<option value='" . sprintf("%02d", $i) . "'" . ($i==$selectedMin?" selected":"") . ">" . sprintf("%02d", $i) . "</option>\n";
		}
		$html .= "</select>";
		
		return $html;
	}
	
    /**
     * Calculate kwhs and costs for a range of days
     */	
     public function calculateRangeKwhCosts($beginDate, $endDate){
     	 
		return $this->calculateTimeRangeKwhCosts(date ("Y-m-d 00:00:00", strtotime($beginDate)),date ("Y-m-d 00:00:00", strtotime ("+1 day", strtotime($endDate))) );

     }
     
    /**
     * Calculate kwhs and costs for specific day
     */	
     public function calculateDayKwhCosts($checkDate){
     	
		return $this->calculateTimeRangeKwhCosts(date ("Y-m-d 00:00:00", strtotime($checkDate)),date ("Y-m-d 00:00:00", strtotime ("+1 day", strtotime($checkDate))) );

     }     
     
     /*
	 * Calculate kwhs and costs for specific day
     */	
     public function calculateYearKwhCosts($checkDate){
     	
		return $this->calculateTimeRangeKwhCosts(date ("Y-m-d 00:00:00", strtotime($checkDate)),date ("Y-m-d 00:00:00", strtotime ("-1 year", strtotime($checkDate))) );

     }     
	 public function calculateTimeRangeKwhCosts($beginDate, $endDate){

	      	$this->db = new Database();
			$settings = $this->db->getSettings();

			$data = array(
				'kwh' => 0,
				'kwhLow' => 0,
				'price' => 0,
				'priceLow' => 0,
				'priceTotal' => 0,
				'kwhTotal' => 0
			);

			$rows = $this->db->getSpecificTimeRange($beginDate, $endDate);
//			file_put_contents('php://stderr', print_r($rows, TRUE));
			
			foreach($rows as $k) {
				if ( $k->islow == 0 ) {
					$data['kwh'] = $k->kwh;
					$data['price'] = $k->price;
				} else {
					$data['kwhLow']  = $k->kwh;
					$data['priceLow']  = $k->price;
				}
			}
	
/*			
			
				if ( $this->isLowKwh($k->time) == 0 ) {
					$data['kwh'] = $data['kwh'] + $k->value;
				} else {
					$data['kwhLow'] = $data['kwhLow'] + $k->value;
				}


			$data['kwh'] = ($data['kwh'] /60) / 1000;
			$data['kwhLow'] = ($data['kwhLow'] /60) / 1000;
												
			$data['price'] = $data['kwh'] * (float)$settings['cpkwh'];
			$data['priceLow'] = $data['kwhLow'] * (float)$settings['cpkwh_low'];	
*/			
			
			$data['priceTotal'] = $data['price'] + $data['priceLow'];
			$data['kwhTotal'] = $data['kwh'] + $data['kwhLow'];
                      	 		
		return $data;	
		  
     }

	 
	 /**
     * Determine low/high rate
     */	
     public function isLowKwh($checkDate){
     	
     	$this->db = new Database();
     	$settings = $this->db->getSettings();
		
		$kwhLow = 0;

		if($settings['dualcount'] == 1)
		{

			$getDay = strftime('%u', strtotime($checkDate));
			$rtime = date('Hi',strtotime($checkDate));			
			
			$feestdagen = $this->db->getFeestdagen();

			foreach($feestdagen as $k => $v){
                if (strtotime($v) == strtotime($checkDate))
                {
                       $getDay = '8';
                }
			}
			
			$timeStart = (int)str_replace(":","", $settings['cpkwhlow_start']);
			$timeEnd = (int)str_replace(":","", $settings['cpkwhlow_end']);

			if ($getDay == '6' || $getDay == '7' || $getDay == '8' || $rtime >= $timeStart || $rtime < $timeEnd ){
				$kwhLow = 1;
			}
		}
			
		return $kwhLow;		  
     }     
  
   public function updateDatabase(){

	      	$this->db = new Database();
			$settings = $this->db->getSettings();

			$rows = $this->db->data_m();
						
			foreach($rows as $k) {
			
			if ( $this->isLowKwh($k->time) == 0) {
					$low = FALSE;  
					$tf = (float)$settings['cpkwh'];
				} else {
					$low= TRUE;
					$tf = (float)$settings['cpkwh_low'];
				}
			
			$this->db->addMinuteData( $k->time, $k->unit, $k->delta, $k->value, $low, $tf );
				
			}
                      	 		
		return;	
		  
     }

 
 
 
}

?>