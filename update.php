<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>		
		<title>YouLess - Energy Monitor</title>
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" />
		<link type="text/css" href="css/style.min.css" rel="stylesheet" />
		<link type="text/css" href="css/responsive.css" rel="stylesheet" />		
		<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
		<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
		<script type="text/javascript" src="js/highstock.js"></script>
		<script type="text/javascript" src="js/modules/exporting.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
	</head>
	<body>
		
		
		<div id="topHeader">
		</div>
		<div id="header">
		
			<div id="logo"></div>
		
		</div>
		<div id="container">
			<div id="installDiv">
		
<?php

	$errorMsg = '';
	$ok = true;
	
	if(!file_exists('inc/settings.inc.php'))
	{
		$errorMsg .= '<p class="error"><b>settings.inc.php</b> ontbreekt, pas <b>settings.inc.php.example</b> aan en hernoem deze naar <b>settings.inc.php</b></p>';
		$ok = false;
	}
	include "inc/settings.inc.php";
	include "classes/database.class.php";
	include "classes/generic.class.php";	
//	include "inc/session.inc.php";

	$db = new Database();
	$gen = new Generic();
	$settings = $db->getSettings(); 
	
	echo $errorMsg;
	if($ok)
	{
		
		try {
		    $db = new PDO("mysql:host=".DB_HOST, DB_USER, DB_PASS);
		    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
			
		
		    $succes = $db->exec("
				CREATE TABLE IF NOT EXISTS `".DB_NAME."`. `data_h` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `time` datetime NOT NULL,
				  `unit` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
				  `delta` int(11) NOT NULL,
				  `value` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
				  `inserted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				  PRIMARY KEY (`id`),
				  KEY `time` (`time`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;

				CREATE TABLE IF NOT EXISTS `".DB_NAME."`. `data_m` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `time` datetime NOT NULL,
				  `unit` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
				  `delta` int(11) NOT NULL,
				  `value` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
				  `inserted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				  PRIMARY KEY (`id`),
				  UNIQUE KEY `time` (`time`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;
				
				ALTER TABLE `".DB_NAME."`. `data_m` ADD COLUMN cpKwh   	decimal(10,6);
				ALTER TABLE `".DB_NAME."`. `data_m` ADD COLUMN IsLow   	tinyint(1);
								
				
				CREATE TABLE IF NOT EXISTS `".DB_NAME."`. `kwh_h` (
				  `id` int(11) NOT NULL AUTO_INCREMENT,
				  `kwh` varchar(20) NOT NULL,
				  `inserted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				  PRIMARY KEY (`id`),
				  KEY `inserted` (`inserted`)
				) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;					

				CREATE TABLE  IF NOT EXISTS `".DB_NAME."`. `feestdagen` (
				`id` INT( 11 ) NOT NULL AUTO_INCREMENT ,
				`naam` VARCHAR( 30 ) NOT NULL ,
 				`datum` DATE NOT NULL ,
				PRIMARY KEY (  `id` ) ,
				UNIQUE KEY  `datum` (  `datum` )
				) ENGINE=MYISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT =  'Feestdagen met laag tarief';

				INSERT INTO  `".DB_NAME."`. `feestdagen` (`id`, `naam`, `datum`) VALUES
				(1, 'Nieuwjaarsdag', '2013-01-01'),
				(2, 'Tweede Paasdag', '2013-04-01'),
				(3, 'Eerste Paasdag', '2013-03-31'),
				(4, 'Koningsdag', '2013-04-30'),
				(5, 'Hemelvaartsdag', '2013-05-09'),
				(6, 'Eerste Pinksterdag', '2013-05-19'),
				(7, 'Tweede Pinksterdag', '2013-05-20'),
				(8, 'Eerste Kerstdag', '2013-12-25'),
				(9, 'Tweede Kerstdag', '2013-12-26');
				
				CREATE TABLE `".DB_NAME."`.`meter` (
					`time` datetime NOT NULL ,
					`count` decimal( 10, 3 ) NOT NULL ,
					`islow` tinyint( 1 ) NOT NULL ,
					UNIQUE KEY `time` ( `time` , `islow` )
				) ENGINE = MYISAM DEFAULT CHARSET = utf8;
		
				
																	
		    ");

			$gen->updateDatabase();

			echo "<p style='color:green;'>Update succesvol. Verwijder <b>install.php</b> en <b>update.php</b></p>";

		} catch (PDOException $e) {
		    die(print("<p class='error'>Database error: ". $e->getMessage() ."</p>"));
		}	
			
	}
?>
			</div>
		</div>
	</body>
</html>
